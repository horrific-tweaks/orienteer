package me.jfenn.orienteer.commands

import com.mojang.brigadier.arguments.IntegerArgumentType
import com.mojang.brigadier.builder.LiteralArgumentBuilder
import com.mojang.brigadier.builder.RequiredArgumentBuilder
import me.jfenn.orienteer.Main
import me.jfenn.orienteer.map.BookItemService
import me.jfenn.orienteer.score.HintService
import me.jfenn.orienteer.state.GameState
import me.jfenn.orienteer.utils.sendInfoMessage
import me.jfenn.orienteer.utils.sendWarningMessage
import net.minecraft.server.command.ServerCommandSource

object HintCommand {

    fun init() = registerCommands {
        LiteralArgumentBuilder.literal<ServerCommandSource>("orienteer")
            .then(
                LiteralArgumentBuilder.literal<ServerCommandSource>("hint")
                    .requireState(GameState.PLAYING)
                    .then(
                        RequiredArgumentBuilder.argument<ServerCommandSource?, Int?>("flag", IntegerArgumentType.integer())
                            .executes {
                                val ctx = Main.getContext(it.source.server) ?: return@executes 0
                                val player = it.source.player ?: return@executes 0
                                val flagIndex = IntegerArgumentType.getInteger(it, "flag")

                                val hintCount = HintService.getHintCount(ctx, player, flagIndex)
                                val penalty = HintService.getHintPenalty(ctx, hintCount)

                                try {
                                    HintService.unlockHint(ctx, player, flagIndex)
                                } catch (e: RuntimeException) {
                                    e.message
                                        ?.let { message ->
                                            player.sendWarningMessage(message)
                                        }
                                        ?: e.printStackTrace()

                                    return@executes 0
                                }

                                BookItemService.updateBookItems(ctx, player)
                                player.sendInfoMessage("Unlocked hint ${hintCount+1} for $penalty points!")
                                1
                            }
                    )
            )
            .also { yield(it) }
    }

}