package me.jfenn.orienteer.map

import net.minecraft.block.Blocks
import net.minecraft.world.biome.BiomeKeys

class MapConfig {

    val openBiomeSet = setOf(
        BiomeKeys.DESERT,
        BiomeKeys.BADLANDS,
        BiomeKeys.BEACH,
        BiomeKeys.SNOWY_BEACH,
        BiomeKeys.WINDSWEPT_HILLS,
        BiomeKeys.WINDSWEPT_GRAVELLY_HILLS,
        BiomeKeys.STONY_SHORE,
    )

    val roughOpenBiomeSet = setOf(
        BiomeKeys.MUSHROOM_FIELDS,
        BiomeKeys.PLAINS,
        BiomeKeys.MEADOW,
        BiomeKeys.SUNFLOWER_PLAINS,
        BiomeKeys.SNOWY_PLAINS,
        BiomeKeys.SNOWY_SLOPES,
    )

    val swampBiomeSet = setOf(
        BiomeKeys.SWAMP,
    )

    val forestOpenBiomeSet = setOf(
        BiomeKeys.SPARSE_JUNGLE,
        BiomeKeys.WOODED_BADLANDS,
        BiomeKeys.SAVANNA,
        BiomeKeys.SAVANNA_PLATEAU,
        BiomeKeys.WINDSWEPT_SAVANNA,
    )

    val forestBiomeSet = setOf(
        BiomeKeys.BIRCH_FOREST,
        BiomeKeys.CHERRY_GROVE,
        BiomeKeys.FLOWER_FOREST,
        BiomeKeys.FOREST,
        BiomeKeys.OLD_GROWTH_BIRCH_FOREST,
        BiomeKeys.WINDSWEPT_FOREST,
        BiomeKeys.TAIGA,
        BiomeKeys.SNOWY_TAIGA,
        BiomeKeys.OLD_GROWTH_PINE_TAIGA,
        BiomeKeys.OLD_GROWTH_SPRUCE_TAIGA,
        BiomeKeys.GROVE,
    )

    val forestRoughBiomeSet = setOf(
        BiomeKeys.DARK_FOREST,
        BiomeKeys.JUNGLE,
        BiomeKeys.BAMBOO_JUNGLE,
        BiomeKeys.MANGROVE_SWAMP,
    )

    val ignoredStructures = setOf(
        "minecraft:buried_treasure"
    )

    val pathStructurePieces = setOf(
        ""
    )

    val pathBlockSet = setOf(
        Blocks.DIRT_PATH,
    )

}