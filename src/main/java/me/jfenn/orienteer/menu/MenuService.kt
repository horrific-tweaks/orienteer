package me.jfenn.orienteer.menu

import me.jfenn.orienteer.Main
import me.jfenn.orienteer.config.BlockPosition
import me.jfenn.orienteer.config.Config
import me.jfenn.orienteer.state.GameState
import me.jfenn.orienteer.state.OrienteerContext
import net.fabricmc.fabric.api.networking.v1.ServerPlayConnectionEvents
import net.minecraft.block.entity.StructureBlockBlockEntity
import net.minecraft.nbt.NbtCompound
import net.minecraft.nbt.NbtIo
import net.minecraft.nbt.NbtTagSizeTracker
import net.minecraft.server.world.ChunkTicketType
import net.minecraft.server.world.ServerWorld
import net.minecraft.structure.StructurePlacementData
import net.minecraft.util.Unit
import net.minecraft.util.math.BlockPos
import net.minecraft.util.math.ChunkPos
import org.slf4j.LoggerFactory
import java.nio.file.Files
import kotlin.io.path.name
import kotlin.system.measureTimeMillis

object MenuService {

    private val log = LoggerFactory.getLogger(this::class.java)

    fun init() {
        GameState.PREGAME.onEnter {
            // this *shouldn't* happen... just being safe to prevent double-entities
            if (menu != null) {
                log.error("BUG: GameState.PREGAME onEnter handler has been invoked already!")
                return@onEnter
            }

            // always load the spawn chunks
            lobbyWorld.chunkManager.addTicket(ChunkTicketType.START, ChunkPos(0, 0), 11, Unit.INSTANCE)

            spawnLobbyStructure(this, lobbyWorld)
            menu = MenuInstance(this, state.lobbySpawnPos)
        }

        Main.onGameTick {
            menu?.tick()
        }

        GameState.PLAYING.onEnter {
            menu?.cleanup()
            menu = null
        }

        ServerPlayConnectionEvents.JOIN.register { _, _, server ->
            val ctx = Main.getContext(server)
            ctx?.menu?.markDirty()
        }

        ServerPlayConnectionEvents.DISCONNECT.register { _, server ->
            val ctx = Main.getContext(server)
            ctx?.menu?.markDirty()
        }
    }

    private fun spawnLobbyStructure(ctx: OrienteerContext, lobbyWorld: ServerWorld) = measureTimeMillis {
        val lobbyDir = Config.configDir.resolve("lobby")
        if (!Files.exists(lobbyDir))
            lobbyDir.toFile().mkdirs()

        val lobbyFile = Files.list(lobbyDir)
            .map { it.name }
            .toList()
            .toMutableSet()
            .apply { add("lobby.nbt") }
            .random()

        log.info("Spawning lobby structure '$lobbyFile'")

        // get a random NBT structure from the config, and read as string
        val lobbyFileStream = lobbyDir.resolve(lobbyFile)
            .toFile()
            .takeIf { it.exists() }
            ?.inputStream()
            ?: run {
                // if there aren't any custom NBT files, read the default (lobby.nbt) from resources
                javaClass.getResourceAsStream("/orienteer/lobby/lobby.nbt")
                    ?: throw RuntimeException("Unable to obtain lobby.nbt resource")
            }

        val lobbyNbt = lobbyFileStream.use { NbtIo.readCompressed(it, NbtTagSizeTracker(Long.MAX_VALUE, 512)) }
        placeStructure(lobbyWorld, BlockPos(0, 24, 0), lobbyNbt)

        // Search for spawnpoint
        val spawnY = (0..24)
            .asSequence()
            .filter { y -> lobbyWorld.getBlockState(BlockPos(0, y, 0)).isSolidBlock(lobbyWorld, BlockPos.ORIGIN) }
            .max()

        ctx.state.lobbySpawnPos = BlockPosition(0, spawnY+1, 0)
        log.info("Set lobby spawnpoint to ${ctx.state.lobbySpawnPos}")
    }.also {
        log.info("Done (${it}ms)!")
    }

    private fun placeStructure(world: ServerWorld, pos: BlockPos, nbt: NbtCompound) {
        val template = world.structureTemplateManager.createTemplate(nbt)
        val placement = StructurePlacementData()

        val posOffset = template.size.run { pos.add(-x/2, -y/2, -z/2) }

        template.place(
            world,
            posOffset,
            posOffset,
            placement,
            StructureBlockBlockEntity.createRandom(world.seed),
            2
        )
    }

}