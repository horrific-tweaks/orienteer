package me.jfenn.orienteer.commands

import com.mojang.brigadier.builder.LiteralArgumentBuilder
import me.jfenn.orienteer.Main
import me.jfenn.orienteer.config.BlockPosition
import me.jfenn.orienteer.map.FlagEntityInfo
import me.jfenn.orienteer.map.FlagPositionInfo
import net.minecraft.server.command.ServerCommandSource

object TestCommand {

    fun init() = registerCommands {

        LiteralArgumentBuilder.literal<ServerCommandSource>("spawn-control")
            .executes {
                val ctx = Main.getContext(it.source.server) ?: return@executes 0
                FlagEntityInfo.spawn(
                    ctx, it.source.world,
                    FlagPositionInfo(
                        with(it.source.position) {
                            BlockPosition(x.toInt(), y.toInt(), z.toInt())
                        },
                        biomeId = null,
                        blockId = null,
                        structureId = null,
                        isUnderground = false,
                        isStart = false,
                        isOnGround = false,
                    ),
                    3
                )
                1
            }
            .also { yield(it) }
    }

}