package me.jfenn.orienteer.map

import net.minecraft.text.Text

class ProgressInfo(
    val description: String,
    val currentProgress: Int,
    val maxProgress: Int,
) {

    fun formatText() = Text.empty()
        .append(description)
        .append(" ${((currentProgress.toFloat() / maxProgress) * 100).toInt()}% ($currentProgress / $maxProgress)")

}
