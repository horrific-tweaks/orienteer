package me.jfenn.orienteer.integrations

import me.jfenn.orienteer.Main
import me.jfenn.orienteer.Sounds
import me.jfenn.orienteer.event.InteractionEntityEvents
import me.jfenn.orienteer.map.BookItemService
import me.jfenn.orienteer.map.FlagCaptureInfo
import me.jfenn.orienteer.map.FlagService
import me.jfenn.orienteer.map.MapRenderService
import me.jfenn.orienteer.state.GameState
import net.minecraft.particle.ParticleTypes

object FlagController {

    fun init() {
        GameState.PLAYING.onEnter { prevState ->
            if (prevState == GameState.PLAYING) return@onEnter

            FlagService.spawnFlags(ctx)
        }

        InteractionEntityEvents.onTick { entity ->
            val ctx = entity.server?.let { Main.getContext(it) } ?: return@onTick
        }

        InteractionEntityEvents.onInteract { (entity, player) ->
            val ctx = entity.server?.let { Main.getContext(it) } ?: return@onInteract

            // Find the flag postion that corresponds with the interaction entity (by UUID)
            val (flagIndex, flagPosition) = ctx.state.flagPositions
                .withIndex()
                .find { (_, pos) -> pos.entityInfo?.interactionEntity == entity.uuidAsString }
                ?: run {
                    Sounds.playFlagError(ctx, entity)
                    return@onInteract
                }

            if (ctx.state.hasFlagCapture(player, flagIndex)) {
                Sounds.playFlagError(ctx, entity)
                return@onInteract
            }

            // TODO: Unless we're in SCORE mode, don't capture flags out of order!
            val nextIndex = FlagService.getNextFlag(ctx, player)
            if (flagIndex != nextIndex) {
                Sounds.playFlagError(ctx, entity)
                return@onInteract
            }

            // Mark the flag as captured!
            ctx.state.setFlagCapture(player, flagIndex, FlagCaptureInfo())

            val entityInfo = flagPosition.entityInfo!!

            // spin the flag!
            Sounds.playFlagCaptured(ctx, entity)
            entityInfo.updateTransformation(ctx)
            entityInfo.getWorld(ctx.server)
                ?.spawnParticles(ParticleTypes.END_ROD, entity.x, entity.y + 0.5, entity.z, 10, 0.5, 0.5, 0.5, 0.01)

            // Update the player's book item
            BookItemService.updateBookItems(ctx, player)
            MapRenderService.update(ctx, player)
        }
    }

}