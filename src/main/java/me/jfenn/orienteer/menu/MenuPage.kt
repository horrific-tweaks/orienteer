package me.jfenn.orienteer.menu

import me.jfenn.orienteer.OrienteerGame
import me.jfenn.orienteer.state.OrienteerContext
import net.minecraft.util.math.Vec3d

fun MenuComponent.registerMenuPage(
    position: Vec3d,
    ctx: OrienteerContext,
) {

    var page = 0

    val pages = listOf(
        component {
            registerTimer(
                title = "Time Limit",
                timerMinutesProp = propertyRef(ctx.config::timeLimit),
                position = position.add(2.3, 0.0, 0.0),
            )
        },
        component {
            registerSpawnKit(
                position = position.add(-1.5, 0.0, 0.0),
                config = ctx.config,
            )
        },
        component {
        },
    )

    for (i in pages.indices) {
        registerButton(
            position = position.add(-((pages.size-1) * 0.7) + (i*1.4), 0.5, 0.0),
            text = "Page ${i+1}",
            isActiveProp = computedProperty { page == i },
        ) {
            page = i
            markDirty()
        }
    }

    registerButton(
        position = position.add(5.0, -MENU_ITEM_OFFSET, 0.0),
        text = "START",
        isActiveProp = computedProperty { true },
    ) { player ->
        OrienteerGame.start(ctx)
    }

    onTick { instance ->
        for (i in pages.indices) {
            if (page == i) {
                pages[i].tick(instance)
            }
        }
    }

    onUpdate { instance ->
        for (i in pages.indices) {
            if (page == i) {
                pages[i].spawn(instance)
            } else {
                pages[i].despawn()
            }
        }
    }

    onDespawn {
        pages.forEach { it.despawn() }
    }
}