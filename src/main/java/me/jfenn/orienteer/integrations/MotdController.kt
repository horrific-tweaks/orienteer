package me.jfenn.orienteer.integrations

import me.jfenn.orienteer.state.OrienteerContext
import me.jfenn.orienteer.state.GameState

object MotdController {

    fun updateMotd(ctx: OrienteerContext) {
        val state = ctx.state.state
        ctx.server.setMotd("${state.color}[${state.motd}]§f Orienteering")
    }

    fun init() {
        for (state in GameState.entries) {
            state.onEnter {
                updateMotd(this)
            }
        }
    }

}