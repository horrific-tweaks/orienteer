package me.jfenn.orienteer.config

import kotlinx.serialization.Serializable
import net.minecraft.util.math.BlockPos
import net.minecraft.util.math.ChunkPos
import kotlin.math.pow
import kotlin.math.sqrt

@Serializable
data class BlockPosition(
    val x: Int,
    val y: Int,
    val z: Int,
) {
    companion object {

        fun fromBlockPos(pos: BlockPos) = BlockPosition(pos.x, pos.y, pos.z)

    }

    fun toBlockPos() = BlockPos(x, y, z)

    fun toChunkPos() = ChunkPos(toBlockPos())

    override fun toString(): String {
        return "[$x, $y, $z]"
    }

}

fun BlockPos.toBlockPosition() = BlockPosition.fromBlockPos(this)

fun BlockPos.distance2d(other: BlockPos): Float {
    return sqrt(
        (x - other.x).toFloat().pow(2) +
                (z - other.z).toFloat().pow(2)
    )
}

fun BlockPos.distance3d(other: BlockPos): Float {
    return sqrt(
        (x - other.x).toFloat().pow(2) +
                (y - other.y).toFloat().pow(2) +
                (z - other.z).toFloat().pow(2)
    )
}

operator fun BlockPos.component1() = x
operator fun BlockPos.component2() = y
operator fun BlockPos.component3() = z

fun BlockPos.toChunkPos(): ChunkPos {
    return ChunkPos(this)
}

fun ChunkPos.distanceTo(other: ChunkPos): Float {
    return sqrt(
        (x - other.x).toFloat().pow(2) +
                (z - other.z).toFloat().pow(2)
    )
}

operator fun ChunkPos.component1() = x
operator fun ChunkPos.component2() = z
