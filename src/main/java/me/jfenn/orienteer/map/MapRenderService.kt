package me.jfenn.orienteer.map

import me.jfenn.orienteer.config.toChunkPos
import me.jfenn.orienteer.spawn.isFluidBlock
import me.jfenn.orienteer.state.OrienteerContext
import net.minecraft.item.map.MapIcon
import net.minecraft.item.map.MapState
import net.minecraft.registry.RegistryKeys
import net.minecraft.server.network.ServerPlayerEntity
import net.minecraft.server.world.ServerWorld
import net.minecraft.text.Text
import net.minecraft.util.Formatting
import net.minecraft.util.math.BlockPos
import net.minecraft.util.math.ChunkPos
import net.minecraft.world.Heightmap
import net.minecraft.world.biome.BiomeKeys
import net.minecraft.world.gen.GenerationStep
import net.minecraft.world.gen.structure.BuriedTreasureStructure
import net.minecraft.world.gen.structure.JigsawStructure
import org.slf4j.LoggerFactory
import kotlin.jvm.optionals.getOrNull
import kotlin.math.absoluteValue
import kotlin.math.max
import kotlin.math.roundToInt
import kotlin.system.measureTimeMillis

object MapRenderService {

    private val log = LoggerFactory.getLogger(this::class.java)

    private val COLOR_NONE = Color(255, 255, 255)
    // private val COLOR_CONTOUR = Color(216, 127, 51)
    // private val COLOR_CONTOUR_DARK = Color(186, 109, 44)
    // private val COLOR_CONTOUR_DARKER = Color(152, 89, 36)
    private val COLOR_CONTOUR = Color(216, 127, 51)
    private val COLOR_CONTOUR_DARK = Color(186, 109, 44)
    private val COLOR_CONTOUR_DARKER = Color(152, 89, 36)
    private val COLOR_RIVER = Color(160, 160, 255)
    private val COLOR_OCEAN = Color(138, 138, 220)
    private val COLOR_OCEAN_DEEP = Color(112, 112, 180)
    private val COLOR_PATH = Color(230, 180, 120)
    private val COLOR_BUILDING = Color(61, 64, 64)
    private val COLOR_BIOME_OPEN = Color(254, 216, 143)
    private val COLOR_BIOME_ROUGH_OPEN = Color(255, 226, 183)
    private val COLOR_BIOME_FOREST_OPEN = Color(127, 204, 25)
    private val COLOR_BIOME_FOREST = Color(127, 178, 56)
    private val COLOR_BIOME_FOREST_ROUGH = Color(89, 144, 17)

    private val COLOR_CONTROL = Color(220, 0, 0)

    private val config = MapConfig()

    const val MAP_SIZE = 128
    const val MAP_SCALE = 8

    fun allMapChunks() = buildList {
        val chunkMin = getTerrainPos(Pair(0, 0)).toChunkPos()
        val chunkMax = getTerrainPos(Pair(127, 127)).toChunkPos()

        for (x in chunkMin.x..chunkMax.x) {
            for (z in chunkMin.z..chunkMax.z) {
                add(ChunkPos(x, z))
            }
        }
    }

    fun getMapPos(mapIndex: Int) : Pair<Int, Int> {
        return Pair(mapIndex % MAP_SIZE, mapIndex / MAP_SIZE)
    }

    fun getTerrainPos(mapPos: Pair<Int, Int>) : BlockPos {
        val (mapX, mapY) = mapPos
        val terrainX = (mapX - MAP_SIZE/2) * MAP_SCALE
        val terrainZ = (mapY - MAP_SIZE/2) * MAP_SCALE
        return BlockPos(terrainX, 0, terrainZ)
    }

    fun getMapIndexForBlock(terrainX: Int, terrainZ: Int) : Int {
        val mapX = (terrainX / MAP_SCALE) + (MAP_SIZE/2)
        val mapY = (terrainZ / MAP_SCALE) + (MAP_SIZE/2)
        return getMapIndex(mapX, mapY)
    }

    fun getMapIndex(mapX: Int, mapY: Int) : Int {
        return mapX + mapY * MAP_SIZE
    }

    fun createTerrainMap(ctx: OrienteerContext, world: ServerWorld, chunkAccess: CachedChunkAccess) = sequence {
        val state = ctx.terrainMap

        val scale = MAP_SCALE
        val contourScale = scale * 3

        for (mapIndex in 0 until state.colors.size) {
            yield(ProgressInfo(
                description = "Pre-rendering terrain...",
                currentProgress = mapIndex + 1,
                maxProgress = state.colors.size,
            ))

            val mapPos = getMapPos(mapIndex)
            val (mapX, mapY) = mapPos

            val blockPos = getTerrainPos(mapPos).mutableCopy()
            blockPos.y = chunkAccess.getBlockHeight(blockPos, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES)

            if (blockPos.y < world.seaLevel) {
                val blockState = chunkAccess.getBlockState(blockPos)
                val isFluid = isFluidBlock(blockState)
                val oceanHeight = chunkAccess.getBlockHeight(blockPos, Heightmap.Type.OCEAN_FLOOR)
                state.colors[mapIndex] = when {
                    isFluid && oceanHeight < world.seaLevel - 20 -> COLOR_OCEAN_DEEP.asByte
                    isFluid && oceanHeight < world.seaLevel - 10 -> COLOR_OCEAN.asByte
                    isFluid -> COLOR_RIVER.asByte
                    blockPos.y < world.seaLevel - 2 -> COLOR_NONE.asByte
                    else -> COLOR_BIOME_OPEN.asByte
                }

                continue
            }

            val adjacentHeights = listOf(
                chunkAccess.getBlockHeight(getTerrainPos(mapX + 1 to mapY), Heightmap.Type.MOTION_BLOCKING_NO_LEAVES),
                chunkAccess.getBlockHeight(getTerrainPos(mapX - 1 to mapY), Heightmap.Type.MOTION_BLOCKING_NO_LEAVES),
                chunkAccess.getBlockHeight(getTerrainPos(mapX to mapY + 1), Heightmap.Type.MOTION_BLOCKING_NO_LEAVES),
                chunkAccess.getBlockHeight(getTerrainPos(mapX to mapY - 1), Heightmap.Type.MOTION_BLOCKING_NO_LEAVES),
            )

            var biome = world.getBiome(blockPos).key.getOrNull()
            if (biome == BiomeKeys.WOODED_BADLANDS && blockPos.y < 100) {
                // Hacky case for wooded badlands, as it doesn't spawn trees below y=100
                //   so we should just treat it as badlands instead
                biome = BiomeKeys.BADLANDS
            }

            val isContour = adjacentHeights.any { (it / contourScale) > (blockPos.y / contourScale) }
            if (isContour) {
                state.colors[mapIndex] = when {
                    config.forestBiomeSet.contains(biome) -> COLOR_CONTOUR_DARK.asByte
                    config.forestRoughBiomeSet.contains(biome) -> COLOR_CONTOUR_DARKER.asByte
                    else -> COLOR_CONTOUR.asByte
                }
                continue
            }

            state.colors[mapIndex] = when {
                config.openBiomeSet.contains(biome) -> COLOR_BIOME_OPEN.asByte
                config.roughOpenBiomeSet.contains(biome) -> COLOR_BIOME_ROUGH_OPEN.asByte
                config.forestOpenBiomeSet.contains(biome) -> COLOR_BIOME_FOREST_OPEN.asByte
                config.forestBiomeSet.contains(biome) -> COLOR_BIOME_FOREST.asByte
                config.forestRoughBiomeSet.contains(biome) -> COLOR_BIOME_FOREST_ROUGH.asByte
                config.swampBiomeSet.contains(biome) -> {
                    if (mapY % 2 == 0) COLOR_RIVER.asByte else COLOR_BIOME_ROUGH_OPEN.asByte
                }
                else -> COLOR_NONE.asByte
            }
        }

        val allChunks = allMapChunks()
        for ((chunkIndex, chunkPos) in allChunks.withIndex()) {
            val progress = ProgressInfo(
                description = "Pre-rendering structures...",
                currentProgress = chunkIndex + 1,
                maxProgress = allChunks.size,
            )
            yield(progress)

            val structureRegistry = ctx.server.registryManager.get(RegistryKeys.STRUCTURE)
            // val structurePieceRegistry = ctx.server.registryManager.get(RegistryKeys.STRUCTURE_PIECE)

            for ((structure, structureStart) in chunkAccess.getChunk(chunkPos).structureStarts) {
                val structureId = structureRegistry.getId(structure)
                if (config.ignoredStructures.contains(structureId.toString()))
                    continue

                if (structure.featureGenerationStep == GenerationStep.Feature.SURFACE_STRUCTURES) {
                    if (structure is BuriedTreasureStructure)
                        continue

                    for (child in structureStart.children) {
                        val rangeX = child.boundingBox.minX..child.boundingBox.maxX
                        val rangeZ = child.boundingBox.minZ..child.boundingBox.maxZ

                        // val structurePieceId = structurePieceRegistry.getId(child.type)

                        for (x in rangeX) for (z in rangeZ) {
                            yield(progress)

                            val index = getMapIndexForBlock(x, z)
                            if (index !in state.colors.indices)
                                continue

                            if (structure !is JigsawStructure) {
                                state.colors[index] = COLOR_BUILDING.asByte
                                continue
                            }

                            val blockHeight = chunkAccess.getBlockHeight(BlockPos(x, 0, z), Heightmap.Type.MOTION_BLOCKING_NO_LEAVES)
                            val blockState = chunkAccess.getBlockState(BlockPos(x, blockHeight, z))

                            state.colors[index] = when {
                                config.pathBlockSet.contains(blockState.block) -> COLOR_BIOME_OPEN.asByte
                                else -> COLOR_BUILDING.asByte
                            }
                        }
                    }
                }
            }
        }

        log.info("Finished pre-rendering the terrain map!")
    }

    fun drawLine(
        state: MapState,
        fromPos: Pair<Int, Int>,
        toPos: Pair<Int, Int>,
    ) {
        val (fromX, fromY) = fromPos
        val (toX, toY) = toPos

        val dx = toX - fromX
        val dy = toY - fromY

        val tMax = max(dx.absoluteValue, dy.absoluteValue)
        val tx = dx.toFloat() / tMax
        val ty = dy.toFloat() / tMax

        (2..tMax-2)
            .map { t ->
                val x = (t * tx).roundToInt() + fromX
                val y = (t * ty).roundToInt() + fromY
                Pair(x, y)
            }
            .filter {  (x, y) ->
                listOf(
                    getMapIndex(x, y + 1),
                    getMapIndex(x, y - 1),
                    getMapIndex(x + 1, y),
                    getMapIndex(x + 1, y + 1),
                    getMapIndex(x + 1, y - 1),
                    getMapIndex(x - 1, y),
                    getMapIndex(x - 1, y + 1),
                    getMapIndex(x - 1, y - 1),
                ).none {
                    it in state.colors.indices &&
                            state.colors[it] == COLOR_CONTROL.asByte
                }
            }
            .forEach { (x, y) ->
                val i = getMapIndex(x, y)
                if (i in state.colors.indices)
                    state.colors[i] = COLOR_CONTROL.asByte
            }
    }

    fun update(
        ctx: OrienteerContext,
        player: ServerPlayerEntity,
        mapName: String = MapItemService.getMapName(ctx, player),
    ) = measureTimeMillis {
        log.info("Updating $mapName for ${player.nameForScoreboard}...")
        val state = ctx.terrainMap.copy()

        for ((flagIndex, flagPosition) in ctx.state.flagPositions.withIndex()) {
            val mapIndex = with(flagPosition.position) { getMapIndexForBlock(x, z) }
            val (mapX, mapY) = getMapPos(mapIndex)

            // Draw the control point on the map
            when {
                flagIndex == 0 -> {
                    state.addIcon(
                        MapIcon.Type.PLAINS_VILLAGE,
                        ctx.spawnWorld,
                        "spawn",
                        mapX * 2.0 - MAP_SIZE,
                        mapY * 2.0 - MAP_SIZE,
                        180.0,
                        null,
                    )
                }
                !flagPosition.isStart -> {
                    state.addIcon(
                        MapIcon.Type.PLAYER_OFF_LIMITS,
                        ctx.spawnWorld,
                        "control-$flagIndex",
                        mapX * 2.0 - MAP_SIZE,
                        mapY * 2.0 - MAP_SIZE,
                        180.0,
                        Text.literal("$flagIndex")
                            .formatted(when {
                                ctx.state.hasFlagCapture(player, flagIndex) -> Formatting.GREEN
                                else -> Formatting.WHITE
                            }),
                    )
                }
            }

            // Draw a line between this and the next flag
            if (!ctx.config.isScoreMode) {
                val nextFlag = ctx.state.flagPositions.getOrNull(flagIndex + 1)
                    ?: ctx.state.flagPositions.first()

                drawLine(
                    state = state,
                    fromPos = getMapPos(with(nextFlag.position) { getMapIndexForBlock(x, z) }),
                    toPos = Pair(mapX, mapY),
                )
            }
        }

        state.isDirty = false
        ctx.server.overworld.putMapState(mapName, state)
    }.also {
        log.info("Re-drawing $mapName for ${player.nameForScoreboard} (${it}ms)")
    }

}