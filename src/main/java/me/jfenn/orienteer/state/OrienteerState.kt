package me.jfenn.orienteer.state

import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlinx.serialization.Serializable
import me.jfenn.orienteer.config.BlockPosition
import me.jfenn.orienteer.config.ConfigService
import me.jfenn.orienteer.config.OrienteerConfig
import me.jfenn.orienteer.map.FlagCaptureInfo
import me.jfenn.orienteer.map.FlagEntityInfo
import me.jfenn.orienteer.map.FlagPositionInfo
import me.jfenn.orienteer.utils.formatString
import net.minecraft.server.network.ServerPlayerEntity
import net.minecraft.text.Text
import net.minecraft.util.Formatting
import kotlin.time.Duration

@Serializable
data class OrienteerState(
    var state: GameState = GameState.PREGAME,
    var startedAt: Instant? = null,
    var updatedAt: Instant? = null,
    var lobbySpawnPos: BlockPosition = BlockPosition(0, 0, 0),
    var timeOffline: Duration = Duration.ZERO,
    var config: OrienteerConfig = ConfigService.config,
    /** index of player.uuid -> mapId */
    var playerMapIds: MutableMap<String, Int> = mutableMapOf(),
    /** index of player.uuid -> flagIndex -> hintCount */
    var playerHints: MutableMap<String, MutableMap<Int, Int>> = mutableMapOf(),
    var flagPositions: MutableList<FlagPositionInfo> = mutableListOf(),
    /** index of player.uuid -> flagIndex -> capture */
    var flagCaptures: MutableMap<String, MutableMap<Int, FlagCaptureInfo>> = mutableMapOf(),
) : SerializedPersistentState() {

    fun hasFlagCapture(player: ServerPlayerEntity, flagIndex: Int): Boolean {
        return flagCaptures[player.uuidAsString]?.containsKey(flagIndex) == true
    }

    fun setFlagCapture(player: ServerPlayerEntity, flagIndex: Int, capture: FlagCaptureInfo) {
        val flagCaptureMap = flagCaptures.getOrPut(player.uuidAsString) { mutableMapOf() }
        flagCaptureMap[flagIndex] = capture
    }

    fun changeState(ctx: OrienteerContext, newState: GameState) {
        val prevState = this.state
        this.state = newState
        newState.onEnter(ctx, prevState)
    }

    fun ingameDuration(): Duration? {
        val started = startedAt ?: return null
        val duration = Clock.System.now() - started
        // subtract any offline server time from the game duration
        return duration - timeOffline
    }

    fun formatTimeRemaining(): Text {
        val timeLimit = config.timeLimit
            ?: return Text.literal("(No Time Limit)").formatted(Formatting.BOLD)

        val duration = ingameDuration() ?: return Text.literal(timeLimit.formatString())
        val secondsRemaining = timeLimit - duration

        if (duration >= timeLimit || state == GameState.POSTGAME)
            return Text.literal("(Game Over)").formatted(Formatting.BOLD)

        return Text.literal(secondsRemaining.formatString())
            .formatted(Formatting.BOLD, if (secondsRemaining.inWholeSeconds <= 30 && secondsRemaining.inWholeSeconds % 2 == 0L) { Formatting.RED } else { Formatting.WHITE })
    }

}