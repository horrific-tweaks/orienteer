package me.jfenn.orienteer.spawn

import me.jfenn.orienteer.config.OrienteerConfig
import me.jfenn.orienteer.state.OrienteerContext
import me.jfenn.orienteer.utils.giveOrEquipStack
import net.minecraft.server.network.ServerPlayerEntity
import net.minecraft.server.world.ServerWorld
import org.slf4j.LoggerFactory
import kotlin.random.Random

object SpawnService {

    private val log = LoggerFactory.getLogger(this::class.java)

    fun getSpawnDimension(ctx: OrienteerContext): ServerWorld {
        return ctx.server.worlds.find {
            it.registryKey.value.toString() == ctx.config.spawnDimension
        } ?: run {
            log.error("Could not find spawnDimension '${ctx.config.spawnDimension}'; defaulting to minecraft:overworld")
            ctx.server.overworld
        }
    }

    fun teleportPlayer(ctx: OrienteerContext, player: ServerPlayerEntity) {
        // random offset for each player to prevent collision grouping
        val world = getSpawnDimension(ctx)
        // TODO: change this to a start structure spawn pos?
        val spawn = world.spawnPos

        player.teleport(world, spawn.x + Random.nextDouble(), spawn.y.toDouble(), spawn.z + Random.nextDouble(), 0f, 0f)
        player.setSpawnPoint(
            world.registryKey,
            spawn,
            0f, true, false,
        )
    }

    /**
     * Equip the player with any configured spawn equipment
     */
    fun giveSpawnEquipment(config: OrienteerConfig, players: List<ServerPlayerEntity>) {
        if (!config.isPlayerKit) return

        val itemStacks = config.playerKitItems
            .flatMap { it.toItemStacks() }

        for (stack in itemStacks) {
            for (player in players) {
                // copy the stack for each player
                val playerStack = stack.copy()
                // if the item can be equipped (e.g. iron chestplate), put it in an equipment slot
                player.giveOrEquipStack(playerStack)
            }
        }
    }

}