package me.jfenn.orienteer.map

import me.jfenn.orienteer.NBT_ORIENTEER_BOOK
import me.jfenn.orienteer.NBT_ORIENTEER_VANISH
import me.jfenn.orienteer.score.HintService
import me.jfenn.orienteer.state.OrienteerContext
import me.jfenn.orienteer.utils.allHeldStacks
import net.minecraft.item.ItemStack
import net.minecraft.item.Items
import net.minecraft.nbt.NbtList
import net.minecraft.nbt.NbtString
import net.minecraft.server.network.ServerPlayerEntity
import net.minecraft.text.Text

object BookItemService {

    fun isBookItem(stack: ItemStack) : Boolean {
        return stack.item == Items.WRITTEN_BOOK
            && stack.nbt?.getBoolean(NBT_ORIENTEER_BOOK) == true
    }

    fun createBookItem(ctx: OrienteerContext, player: ServerPlayerEntity) : ItemStack {
        val item = ItemStack(Items.WRITTEN_BOOK, 1)
        item.getOrCreateNbt().apply {
            putBoolean(NBT_ORIENTEER_VANISH, true)
            putBoolean(NBT_ORIENTEER_BOOK, true)
            putString("title", "Orienteering Hints")
            putString("author", "fennifith")
        }

        updateBook(ctx, player, item)
        return item
    }

    fun updateBook(ctx: OrienteerContext, player: ServerPlayerEntity, item: ItemStack) {
        val nearestFlagIndex = FlagService.getNearestFlag(ctx, player)
            // TODO: empty behavior? (the player has finished the game)
            ?: return

        item.getOrCreateNbt().apply {
            val pages = NbtList()

            HintService.createHintList(ctx, player, nearestFlagIndex)
                .map { Text.Serialization.toJsonString(it) }
                .forEach { pages.add(NbtString.of(it)) }

            put("pages", pages)
        }
    }

    fun updateBookItems(ctx: OrienteerContext, player: ServerPlayerEntity) {
        player.allHeldStacks()
            .filter { isBookItem(it) }
            .forEach { item ->
                updateBook(ctx, player, item)
            }
    }

}