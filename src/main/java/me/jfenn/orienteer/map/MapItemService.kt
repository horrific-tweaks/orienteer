package me.jfenn.orienteer.map

import me.jfenn.orienteer.Main
import me.jfenn.orienteer.NBT_ORIENTEER_VANISH
import me.jfenn.orienteer.state.OrienteerContext
import net.minecraft.item.FilledMapItem
import net.minecraft.item.ItemStack
import net.minecraft.item.Items
import net.minecraft.nbt.NbtCompound
import net.minecraft.nbt.NbtList
import net.minecraft.server.network.ServerPlayerEntity
import net.minecraft.text.Text
import net.minecraft.util.Formatting

object MapItemService {

    fun createMapItem(ctx: OrienteerContext, player: ServerPlayerEntity): ItemStack {
        val item = ItemStack(Items.FILLED_MAP, 1)
        item.getOrCreateNbt().apply {
            putInt("map", getMapId(ctx, player))
            putBoolean(NBT_ORIENTEER_VANISH, true)
            put("display", NbtCompound().apply {
                putString("Name", Text.Serialization.toJsonString(
                    Text.literal("Orienteering Map")
                        .formatted(Formatting.ITALIC)
                ))
            })
        }

        return item
    }

    /**
     * Returns true if the item stack is a team's map card
     */
    fun isMapItem(stack: ItemStack): Boolean {
        return stack.item == Items.FILLED_MAP
                && stack.nbt?.getBoolean(NBT_ORIENTEER_VANISH) == true
    }

    fun isMapItem(ctx: OrienteerContext, stack: ItemStack, player: ServerPlayerEntity): Boolean {
        return isMapItem(stack) && FilledMapItem.getMapId(stack) == getMapId(ctx, player)
    }

    fun getMapId(ctx: OrienteerContext, player: ServerPlayerEntity) : Int {
        return ctx.state.playerMapIds.getOrPut(player.uuidAsString) {
            ctx.server.overworld.nextMapId
        }
    }

    fun getMapName(ctx: OrienteerContext, player: ServerPlayerEntity) : String {
        return "map_${getMapId(ctx, player)}"
    }

}