package me.jfenn.orienteer.utils

import net.minecraft.nbt.NbtCompound
import net.minecraft.nbt.NbtDouble
import net.minecraft.nbt.NbtFloat
import net.minecraft.nbt.NbtInt
import net.minecraft.nbt.NbtList
import net.minecraft.util.math.BlockPos
import net.minecraft.util.math.Vec3d
import org.joml.Matrix4f
import org.joml.Quaternionf
import org.joml.Vector3f

fun BlockPos.toNbt(): NbtCompound {
    return NbtCompound().apply {
        putInt("X", x)
        putInt("Y", y)
        putInt("Z", z)
    }
}

fun Vec3d.toNbt(): NbtList {
    return NbtList().apply {
        add(NbtDouble.of(x))
        add(NbtDouble.of(y))
        add(NbtDouble.of(z))
    }
}

fun Vector3f.toNbt(): NbtList {
    return NbtList().apply {
        add(NbtFloat.of(x))
        add(NbtFloat.of(y))
        add(NbtFloat.of(z))
    }
}

fun List<Float>.toNbt(): NbtList {
    return NbtList()
        .also {
            it.addAll(this.map { value -> NbtFloat.of(value) })
        }
}

fun Quaternionf.toNbt(): NbtList {
    return NbtList().apply {
        add(NbtFloat.of(x))
        add(NbtFloat.of(y))
        add(NbtFloat.of(z))
        add(NbtFloat.of(w))
    }
}

fun Matrix4f.toNbt(): NbtList {
    val list = NbtList()
    for (i in 0 until (4*4)) {
        list.add(NbtFloat.of(getRowColumn(i shr 2, i and 3)))
    }
    return list
}

fun NbtList.toMatrix4f(): Matrix4f {
    val matrix = Matrix4f()
    println("---")
    for (i in 0 until (4*4)) {
        println("value ${i shr 2} ${i and 3} -> ${getFloat(i)}")
        matrix.setRowColumn(i shr 2, i and 3, getFloat(i))
    }
    return matrix
}
