package me.jfenn.orienteer.integrations

import me.jfenn.orienteer.Main
import me.jfenn.orienteer.map.MapRenderService
import me.jfenn.orienteer.map.ProgressInfo
import me.jfenn.orienteer.state.GameState
import net.minecraft.server.command.TickCommand
import java.lang.management.ManagementFactory
import kotlin.time.Duration.Companion.milliseconds

object MapController {

    fun init() {
        Main.onGameTick {
            if (state.state != GameState.PREGAME)
                return@onGameTick

            pollTasks()
        }

        GameState.PLAYING.onEnter { prevState ->
            if (prevState != GameState.PLAYING) {
                println("prevState was $prevState - POLLING TASKS ENDLESSLY")
                while (!pollTasks()) {}
            }

            for (player in server.playerManager.playerList) {
                MapRenderService.update(this, player)
            }
        }
    }

}