package me.jfenn.orienteer.menu

import me.jfenn.orienteer.config.OrienteerConfig
import me.jfenn.orienteer.spawn.SpawnItem
import net.minecraft.entity.EntityType
import net.minecraft.screen.SimpleNamedScreenHandlerFactory
import net.minecraft.text.Text
import net.minecraft.util.math.Vec3d

fun MenuComponent.registerSpawnKit(
    position: Vec3d,
    config: OrienteerConfig,
) {
    registerEntity(EntityType.TEXT_DISPLAY, position) {
        putString("text", "\"Spawn Kit\"")
        putString("billboard", "fixed")
        putString("alignment", "center")
        putInt("background", 0)
        putBoolean("shadow", true)
    }

    registerToggleButton(
        position = position.add(0.0, -(MENU_ITEM_OFFSET), 0.0),
        text = "Player Items",
        toggleProp = propertyRef(config::isPlayerKit)
    )

    registerButton(
        position = position.add(0.0, -(MENU_ITEM_OFFSET + MENU_ITEM_HEIGHT), 0.0),
        text = "Edit...",
    ) { player ->
        player.openHandledScreen(
            SimpleNamedScreenHandlerFactory(
                { syncId, inv, _ ->
                    InventoryScreenHandler(syncId, config.playerKitItems.flatMap { it.toItemStacks() }, inv).apply {
                        onClose { stacks ->
                            config.playerKitItems = stacks.map { SpawnItem.fromItemStack(it) }
                        }
                    }
                },
                Text.literal("Player Items")
            )
        )
    }
}