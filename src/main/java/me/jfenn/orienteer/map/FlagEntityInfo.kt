package me.jfenn.orienteer.map

import kotlinx.serialization.Serializable
import me.jfenn.orienteer.NBT_ORIENTEER_FLAG
import me.jfenn.orienteer.NBT_ORIENTEER_FLAG_ROTATED
import me.jfenn.orienteer.state.OrienteerContext
import me.jfenn.orienteer.utils.toNbt
import net.minecraft.entity.EntityType
import net.minecraft.entity.decoration.DisplayEntity
import net.minecraft.nbt.NbtCompound
import net.minecraft.nbt.NbtList
import net.minecraft.nbt.NbtString
import net.minecraft.server.MinecraftServer
import net.minecraft.server.world.ServerWorld
import net.minecraft.text.Text
import net.minecraft.util.Formatting
import net.minecraft.util.math.Vec3d
import org.joml.Matrix4f
import org.joml.Quaternionf
import org.joml.Vector3f
import java.util.*

@Serializable
data class FlagEntityInfo(
    val worldId: String,
    val innerItemEntity: String,
    val alternateItemEntity: String,
    val outerItemEntity: String,
    val stringBlockEntities: List<String>,
    val textEntities: List<String>,
    val interactionEntity: String,
) {

    fun getWorld(server: MinecraftServer) : ServerWorld? {
        return server.worlds.find { it.registryKey.value.toString() == worldId }
    }

    companion object {
        fun spawn(
            ctx: OrienteerContext,
            world: ServerWorld,
            flagPosition: FlagPositionInfo,
            flagIndex: Int,
        ) : FlagEntityInfo {
            val posY = if (flagPosition.isOnGround) 0.3 else 0.5625
            val pos = with(flagPosition.position) {
                Vec3d(x.toDouble() + 0.5, y.toDouble() + posY, z.toDouble() + 0.5)
            }
            val scale = 0.5f
            val brightness = NbtCompound().apply {
                putInt("block", 15)
                putInt("sky", 15)
            }
            val interpolationDuration = 10

            val innerItem = EntityType.ITEM_DISPLAY.create(world)!!
                .also { world.spawnEntity(it) }
                .also {
                    it.readNbt(NbtCompound().apply {
                        put("Pos", pos.toNbt())
                        put("Rotation", listOf(60f, 0f).toNbt())
                        put("brightness", brightness)
                        put("item", NbtCompound().apply {
                            putString("id", "minecraft:orange_concrete")
                            putInt("Count", 1)
                        })
                        putInt("start_interpolation", 0)
                        putInt("interpolation_duration", interpolationDuration)
                        put("transformation", getCompound("transformation").apply {
                            put("translation", Vector3f(0f).toNbt())
                            put("left_rotation", Quaternionf().toNbt())
                            put("scale", Vector3f(scale).toNbt())
                            put("right_rotation", Quaternionf().toNbt())
                        })
                        putFloat("view_range", 2f)
                    })
                }

            val alternateItem = EntityType.ITEM_DISPLAY.create(world)!!
                .also { world.spawnEntity(it) }
                .also {
                    it.readNbt(NbtCompound().apply {
                        put("Pos", pos.toNbt())
                        put("Rotation", listOf(60f, 0f).toNbt())
                        put("brightness", brightness)
                        put("item", NbtCompound().apply {
                            putString("id", "minecraft:white_concrete")
                            putInt("Count", 1)
                        })
                        putInt("start_interpolation", 0)
                        putInt("interpolation_duration", interpolationDuration)
                        put("transformation", getCompound("transformation").apply {
                            put("translation", Vector3f(0f).toNbt())
                            put("left_rotation", Quaternionf().toNbt())
                            put("scale", Vector3f(scale).toNbt())
                            put("right_rotation", Quaternionf(0.01f, 0.01f, 0.01f, 1f).toNbt())
                        })
                        putFloat("view_range", 2f)
                    })
                }

            val outerItem = EntityType.ITEM_DISPLAY.create(world)!!
                .also { world.spawnEntity(it) }
                .also {
                    it.readNbt(NbtCompound().apply {
                        put("Pos", pos.toNbt())
                        put("Rotation", listOf(60f, 0f).toNbt())
                        put("item", NbtCompound().apply {
                            putString("id", "minecraft:glass")
                            putInt("Count", 1)
                        })
                        putInt("start_interpolation", 0)
                        putInt("interpolation_duration", interpolationDuration)
                        put("transformation", getCompound("transformation").apply {
                            put("translation", Vector3f(0f).toNbt())
                            put("left_rotation", Quaternionf().toNbt())
                            put("scale", Vector3f(scale * 1.05f).toNbt())
                            put("right_rotation", Quaternionf().toNbt())
                        })
                    })
                }

            val stringBlocks = buildList {
                // if the flag is on the ground, don't spawn any strings!
                if (flagPosition.isOnGround) return@buildList

                for (rotation in 0..3) {
                    EntityType.BLOCK_DISPLAY.create(world)!!
                        .also { world.spawnEntity(it) }
                        .also {
                            it.readNbt(NbtCompound().apply {
                                put("Pos", pos.toNbt())
                                put("Rotation", listOf(60f + (rotation * 90f), 0f).toNbt())
                                put(
                                    "transformation",
                                    Matrix4f()
                                        .translate(-0.14f, 0.42f, 0.14f)
                                        .scale(scale * 0.8f)
                                        .rotateY(45f.radians)
                                        .rotateX(30f.radians)
                                        .toNbt()
                                )
                                put("block_state", NbtCompound().apply {
                                    putString("Name", "minecraft:tripwire")
                                })
                                putInt("start_interpolation", 0)
                                putInt("interpolation_duration", interpolationDuration)
                            })
                        }
                        .also { add(it) }
                }
            }

            val textEntities = buildList {
                for (rotation in 0..3) {
                    val offsetX = -scale * 0.3f
                    val offsetY = (if (rotation > 1) offsetX else -offsetX) + offsetX * 0.5f
                    EntityType.TEXT_DISPLAY.create(world)!!
                        .also { world.spawnEntity(it) }
                        .also {
                            it.readNbt(NbtCompound().apply {
                                put("Pos", pos.toNbt())
                                put("Rotation", listOf(60f + (rotation * 90f), 0f).toNbt())
                                putString("text", Text.Serialization.toJsonString(
                                    Text.literal(
                                        when {
                                            flagPosition.isStart -> "⌂"
                                            else -> "$flagIndex"
                                        }
                                    ).formatted(Formatting.GOLD)
                                ))
                                put(
                                    "transformation",
                                    Matrix4f()
                                        .translate(offsetX, offsetY, scale/2f + 0.012f)
                                        .scale(scale)
                                        .toNbt()
                                )
                                putString("billboard", "fixed")
                                putString("alignment", "center")
                                putInt("background", 0)
                                putBoolean("shadow", true)
                                put("brightness", brightness)
                            })
                        }
                        .also { add(it) }
                }
            }

            val interactionEntity = EntityType.INTERACTION.create(world)!!
                .also { world.spawnEntity(it) }
                .also {
                    it.readNbt(NbtCompound().apply {
                        put("Pos", with(flagPosition.position) {
                            Vec3d(x + 0.5, y + 0.0, z + 0.5)
                        }.toNbt())
                        put("Tags", NbtList().apply {
                            add(NbtString.of(NBT_ORIENTEER_FLAG))
                        })
                        putFloat("width", 1f)
                        putFloat("height", 1f)
                        put("transformation", getCompound("transformation").apply {
                            put("translation", Vector3f(-0.14f, 0.42f, 0.14f).toNbt())
                            put("left_rotation", Quaternionf().rotateY(45f.radians).rotateX(30f.radians).toNbt())
                            put("scale", Vector3f(scale * 0.8f).toNbt())
                            put("right_rotation", Quaternionf().toNbt())
                        })
                    })
                }

            return FlagEntityInfo(
                worldId = world.registryKey.value.toString(),
                innerItemEntity = innerItem.uuidAsString,
                alternateItemEntity = alternateItem.uuidAsString,
                outerItemEntity = outerItem.uuidAsString,
                stringBlockEntities = stringBlocks.map { it.uuidAsString },
                textEntities = textEntities.map { it.uuidAsString },
                interactionEntity = interactionEntity.uuidAsString,
            )
        }
    }

    fun updateTransformation(ctx: OrienteerContext) {
        val world = getWorld(ctx.server) ?: return

        val interaction = world.getEntity(interactionEntity.uuid()) ?: throw RuntimeException()
        val isRotated = interaction.commandTags.contains(NBT_ORIENTEER_FLAG_ROTATED)

        if (isRotated) {
            interaction.removeCommandTag(NBT_ORIENTEER_FLAG_ROTATED)
        } else {
            interaction.addCommandTag(NBT_ORIENTEER_FLAG_ROTATED)
        }

        val scale = 0.5f
        val rotationY = if (isRotated) 0f else Math.PI.toFloat()

        val interpolationDuration = 10

        val innerItem = world.getEntity(innerItemEntity.uuid()) ?: throw RuntimeException()
        innerItem.writeNbt(NbtCompound())
            .apply {
                put("transformation", getCompound("transformation").apply {
                    put("translation", Vector3f(0f).toNbt())
                    put("left_rotation", Quaternionf().rotateY(rotationY).toNbt())
                    put("scale", Vector3f(scale).toNbt())
                    put("right_rotation", Quaternionf().toNbt())
                })
                putInt("start_interpolation", 0)
                putInt("interpolation_duration", interpolationDuration)
            }
            .let { innerItem.readNbt(it) }

        val alternateItem = world.getEntity(alternateItemEntity.uuid()) ?: throw RuntimeException()
        alternateItem.writeNbt(NbtCompound())
            .apply {
                put("transformation", getCompound("transformation").apply {
                    put("translation", Vector3f(0f).toNbt())
                    put("left_rotation", Quaternionf().rotateY(rotationY).toNbt())
                    put("scale", Vector3f(scale).toNbt())
                    put("right_rotation", Quaternionf(0.01f, 0.01f, 0.01f, 1f).toNbt())
                })
                putInt("start_interpolation", 0)
                putInt("interpolation_duration", interpolationDuration)
            }
            .let { alternateItem.readNbt(it) }

        val outerItem = world.getEntity(outerItemEntity.uuid()) ?: throw RuntimeException()
        outerItem.writeNbt(NbtCompound())
            .apply {
                put("transformation", getCompound("transformation").apply {
                    put("translation", Vector3f(0f).toNbt())
                    put("left_rotation", Quaternionf().rotateY(rotationY).toNbt())
                    put("scale", Vector3f(scale * 1.05f).toNbt())
                    put("right_rotation", Quaternionf().toNbt())
                })
                putInt("start_interpolation", 0)
                putInt("interpolation_duration", interpolationDuration)
            }
            .let { outerItem.readNbt(it) }
    }

}

private fun String.uuid() = UUID.fromString(this)

private val Float.radians get() = Math.toRadians(this.toDouble()).toFloat()
