package me.jfenn.orienteer.integrations

import kotlinx.datetime.Clock
import me.jfenn.orienteer.Main
import me.jfenn.orienteer.state.GameState

/**
 * Checks when the game time exceeds its time limit, and
 * ends the game when it does.
 */
object TimerCheck {

    fun init() {
        GameState.PLAYING.onEnter { prevState ->
            if (prevState != GameState.PREGAME) return@onEnter

            state.startedAt = Clock.System.now()
        }

        Main.onUpdateTick {
            if (state.state != GameState.PLAYING) return@onUpdateTick
            val duration = state.ingameDuration() ?: return@onUpdateTick
            val timeLimit = config.timeLimit ?: return@onUpdateTick

            // set the last updated timestamp
            state.updatedAt = Clock.System.now()

            if (duration >= timeLimit) {
                // end the game!
                // OrienteerGame.end(this, "Out of time!")
            }
        }
    }

}