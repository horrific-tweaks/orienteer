package me.jfenn.orienteer

import me.jfenn.orienteer.state.GameState
import me.jfenn.orienteer.state.OrienteerContext

object OrienteerGame {

    fun start(ctx: OrienteerContext) {
        ctx.state.changeState(ctx, GameState.PLAYING)
    }

}