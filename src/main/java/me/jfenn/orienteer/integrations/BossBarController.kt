package me.jfenn.orienteer.integrations

import me.jfenn.orienteer.MOD_ID
import me.jfenn.orienteer.Main
import me.jfenn.orienteer.config.ConfigService
import me.jfenn.orienteer.utils.formatTitle
import net.minecraft.entity.boss.BossBar
import net.minecraft.entity.boss.CommandBossBar
import net.minecraft.server.MinecraftServer
import net.minecraft.text.Text
import net.minecraft.util.Formatting
import net.minecraft.util.Identifier

/**
 * Displays bossbar score/timer information for all players.
 */
object BossBarController {

    private val ID = Identifier.of("fennifith", MOD_ID)

    fun getBossbar(server: MinecraftServer): CommandBossBar {
        // if the boss bar already exists, recreate it
        server.bossBarManager.get(ID)
            ?.also { server.bossBarManager.remove(it) }

        return server.bossBarManager.add(ID, Text.literal("Time Remaining"))
            .apply {
                color = BossBar.Color.WHITE
                style = BossBar.Style.PROGRESS
                value = 0
                maxValue = 10_000
            }
    }

    fun init() {
        Main.onUpdateTick {
            val timeLimit = config.timeLimit
            val duration = state.ingameDuration()
            if (timeLimit != null && duration != null) {
                bossBar.value = ((1f - (duration.inWholeSeconds / timeLimit.inWholeSeconds.toFloat())) * bossBar.maxValue).toInt()
            }

            bossBar.name = Text.empty()
                .append(Text.literal("Time Remaining: "))
                .append(state.formatTimeRemaining())
        }
    }

}