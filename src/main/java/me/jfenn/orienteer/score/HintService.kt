package me.jfenn.orienteer.score

import me.jfenn.orienteer.state.OrienteerContext
import net.minecraft.server.network.ServerPlayerEntity
import net.minecraft.text.ClickEvent
import net.minecraft.text.Text
import net.minecraft.util.Formatting

object HintService {

    fun getHintCount(ctx: OrienteerContext, player: ServerPlayerEntity, flagIndex: Int) : Int {
        return ctx.state.playerHints[player.uuidAsString]
            ?.get(flagIndex)
            ?: 0
    }

    fun unlockHint(ctx: OrienteerContext, player: ServerPlayerEntity, flagIndex: Int) {
        val flag = ctx.state.flagPositions.getOrNull(flagIndex)
            ?: throw RuntimeException("Flag #$flagIndex not found...")

        val hintCount = getHintCount(ctx, player, flagIndex)
        if (hintCount !in flag.getHints(ctx).indices)
            throw RuntimeException("All hints for #$flagIndex have been unlocked!")

        val flagHintMap = ctx.state.playerHints.getOrPut(player.uuidAsString) { mutableMapOf() }
        flagHintMap[flagIndex] = flagHintMap[flagIndex]?.plus(1) ?: 1
    }

    fun getHintPenalty(ctx: OrienteerContext, hintIndex: Int) : Int {
        val hintCount = hintIndex + 1
        return ctx.config.hintPenalty * hintCount * hintCount
    }

    fun getTotalHintPenalty(ctx: OrienteerContext, player: ServerPlayerEntity) : Int {
        return ctx.state.playerHints[player.uuidAsString]
            ?.values
            // Sum the hint cost of each flag...
            ?.sumOf { hintCount ->
                // Sum the hint penalty for each hint unlocked on the flag
                (0 until hintCount)
                    .sumOf { getHintPenalty(ctx, it) }
            }
            ?: 0
    }

    fun createHintList(
        ctx: OrienteerContext,
        player: ServerPlayerEntity,
        flagIndex: Int,
    ) : List<Text> {
        val pages = mutableListOf<Text>()

        val flag = ctx.state.flagPositions.getOrNull(flagIndex)
            // this should be an error, tbh...
            ?: return pages

        val hints = flag.getHints(ctx)
        val hintCount = getHintCount(ctx, player, flagIndex)
        val hintPenalty = getHintPenalty(ctx, hintCount)

        pages.add(
            Text.empty()
                .append(
                    Text.literal("Control #$flagIndex")
                        .formatted(Formatting.UNDERLINE)
                )
                .append("\n\n")
                .also { text ->
                    hints.take(hintCount)
                        .forEach { text.append(it).append("\n") }
                }
                .also { text ->
                    Text.literal("\n[Reveal Hint: ${hintPenalty}pts]")
                        .formatted(Formatting.BOLD, Formatting.DARK_GREEN)
                        .styled {
                            it.withClickEvent(
                                ClickEvent(
                                ClickEvent.Action.RUN_COMMAND,
                                "/orienteer hint $flagIndex",
                            )
                            )
                        }
                        .takeIf { hintCount < hints.size }
                        ?.also { text.append(it) }
                }
        )

        return pages
    }

}