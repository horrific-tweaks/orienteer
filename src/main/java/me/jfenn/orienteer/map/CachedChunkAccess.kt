package me.jfenn.orienteer.map

import me.jfenn.orienteer.config.*
import net.minecraft.block.BlockState
import net.minecraft.block.Blocks
import net.minecraft.server.world.ServerWorld
import net.minecraft.util.math.BlockPos
import net.minecraft.util.math.ChunkPos
import net.minecraft.util.math.ChunkSectionPos
import net.minecraft.world.Heightmap
import net.minecraft.world.chunk.Chunk
import java.util.WeakHashMap

class CachedChunkAccess(
    val world: ServerWorld,
) {

    val chunkCache = WeakHashMap<ChunkPos, Chunk>()
    fun getChunk(chunkPos: ChunkPos) : Chunk {
        return chunkCache.getOrPut(chunkPos) {
            world.getChunk(chunkPos.x, chunkPos.z)
        }
    }

    fun getChunkFromBlockPos(blockPos: BlockPos) : Chunk {
        return getChunk(blockPos.toChunkPos())
    }

    private val ignoreHeightBlockSet = setOf(
        Blocks.ACACIA_LOG,
        Blocks.BIRCH_LOG,
        Blocks.CHERRY_LOG,
        Blocks.DARK_OAK_LOG,
        Blocks.OAK_LOG,
        Blocks.JUNGLE_LOG,
        Blocks.SPRUCE_LOG,
        Blocks.MANGROVE_LOG,
        Blocks.MANGROVE_ROOTS,
        Blocks.BAMBOO,
        Blocks.BEE_NEST,
    )

    val blockHeightCache = mutableMapOf<Heightmap.Type, MutableMap<Pair<Int, Int>, Int>>()
    fun getBlockHeight(blockPos: BlockPos, type: Heightmap.Type) : Int {
        val cacheMap = blockHeightCache.getOrPut(type) { mutableMapOf() }
        return cacheMap.getOrPut(Pair(blockPos.x, blockPos.z)) {
            val (x, _, z) = blockPos
            val chunk = getChunkFromBlockPos(blockPos)
            val heightmap = chunk.getHeightmap(type)

            var blockHeight = heightmap.get(ChunkSectionPos.getLocalCoord(x), ChunkSectionPos.getLocalCoord(z)) - 1
            while (blockHeight > world.seaLevel) {
                val pos = BlockPos(x, blockHeight, z)
                val blockState = chunk.getBlockState(pos)
                if (blockState.isSolidBlock(world, pos) && !ignoreHeightBlockSet.contains(blockState.block))
                    break

                blockHeight--
            }

            blockHeight
        }
    }

    fun getBlockState(
        blockPos: BlockPos,
    ) : BlockState {
        return getChunkFromBlockPos(blockPos).getBlockState(blockPos)
    }

}