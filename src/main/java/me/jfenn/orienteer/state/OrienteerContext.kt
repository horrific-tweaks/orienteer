package me.jfenn.orienteer.state

import me.jfenn.orienteer.LOBBY_WORLD_ID
import me.jfenn.orienteer.integrations.BossBarController
import me.jfenn.orienteer.map.CachedChunkAccess
import me.jfenn.orienteer.map.FlagService
import me.jfenn.orienteer.map.MapRenderService
import me.jfenn.orienteer.map.ProgressInfo
import me.jfenn.orienteer.menu.MenuInstance
import net.minecraft.item.map.MapState
import net.minecraft.server.MinecraftServer
import net.minecraft.server.world.ServerWorld
import org.slf4j.LoggerFactory
import kotlin.time.Duration.Companion.milliseconds

class OrienteerContext(
    val server: MinecraftServer,
    val state: OrienteerState,
) {

    private val log = LoggerFactory.getLogger(this::class.java)

    val ctx = this
    val config get() = state.config

    val lobbyWorld by lazy {
        server.worlds.find { it.registryKey.value == LOBBY_WORLD_ID }!!
    }

    val spawnWorld: ServerWorld get() {
        return server.worlds.find {
            it.registryKey.value.toString() == ctx.config.spawnDimension
        } ?: run {
            log.error("Could not find spawnDimension '${ctx.config.spawnDimension}'; defaulting to minecraft:overworld")
            ctx.server.overworld
        }
    }

    val bossBar by lazy { BossBarController.getBossbar(server) }

    val terrainMap = MapState.of(1, true, server.overworld.registryKey)
    var controlPlacementTask: Iterator<ProgressInfo>

    var menu: MenuInstance? = null

    operator fun component1() = server
    operator fun component2() = state

    init {
        val chunkAccess = CachedChunkAccess(spawnWorld)
        controlPlacementTask = sequence {
            yieldAll(MapRenderService.createTerrainMap(ctx, spawnWorld, chunkAccess))
            yieldAll(FlagService.createControlPlacements(ctx, spawnWorld, chunkAccess))
        }.iterator()
    }

    fun pollTasks() : Boolean {
        val timeBudget = System.nanoTime() + 40.milliseconds.inWholeNanoseconds
        var progress: ProgressInfo? = null

        while (System.nanoTime() < timeBudget && controlPlacementTask.hasNext()) {
            progress = controlPlacementTask.next()
        }

        if (server.ticks % 10 == 0) {
            progress?.formatText()?.let {
                server.playerManager.broadcast(it, true)
            }
        }

        return !controlPlacementTask.hasNext()
    }
}
