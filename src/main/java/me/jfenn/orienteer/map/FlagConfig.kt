package me.jfenn.orienteer.map

import net.minecraft.block.Blocks

class FlagConfig {

    val structureBlocks = mapOf(
        "minecraft:mineshaft" to setOf(
            Blocks.OAK_PLANKS,
        ),
        "minecraft:mineshaft_mesa" to setOf(
            Blocks.DARK_OAK_PLANKS,
        ),
        "minecraft:shipwreck" to setOf(
            Blocks.OAK_PLANKS,
            Blocks.SPRUCE_PLANKS,
            Blocks.JUNGLE_PLANKS,
            Blocks.DARK_OAK_PLANKS,
        ),
        "minecraft:shipwreck_beached" to setOf(
            Blocks.OAK_PLANKS,
            Blocks.SPRUCE_PLANKS,
            Blocks.JUNGLE_PLANKS,
            Blocks.DARK_OAK_PLANKS,
        ),
        "minecraft:pillager_outpost" to setOf(
            Blocks.DARK_OAK_PLANKS,
            Blocks.DARK_OAK_LOG,
            Blocks.BIRCH_PLANKS,
            Blocks.COBBLESTONE,
            Blocks.MOSSY_COBBLESTONE,
            Blocks.WHITE_WOOL,
        ),
        "minecraft:ruined_portal" to setOf(
            Blocks.OBSIDIAN,
            Blocks.CRYING_OBSIDIAN,
        ),
        "minecraft:village_desert" to setOf(
            Blocks.SMOOTH_SANDSTONE_SLAB,
        ),
        "minecraft:village_plains" to setOf(
            Blocks.OAK_PLANKS,
            Blocks.OAK_STAIRS,
            Blocks.WHITE_WOOL,
            Blocks.YELLOW_WOOL,
        ),
        "minecraft:village_savanna" to setOf(
            Blocks.ACACIA_STAIRS,
            Blocks.ACACIA_PLANKS,
        ),
        "minecraft:village_snowy" to setOf(
            Blocks.SPRUCE_PLANKS,
            Blocks.PACKED_ICE,
        ),
        "minecraft:village_taiga" to setOf(
            Blocks.SPRUCE_LOG,
        ),
    )

    val terrainBlocks = setOf(
        Blocks.OAK_LEAVES,
        Blocks.BIRCH_LEAVES,
        Blocks.SPRUCE_LEAVES,
        Blocks.CHERRY_LEAVES,
        Blocks.DARK_OAK_LEAVES,
        Blocks.ACACIA_LEAVES,
        // jungle leaves are intentionally excluded...
    )

}