package me.jfenn.orienteer.map

import kotlinx.serialization.Serializable
import me.jfenn.orienteer.config.BlockPosition
import me.jfenn.orienteer.state.OrienteerContext
import net.minecraft.registry.RegistryKeys
import net.minecraft.text.Text
import net.minecraft.util.Formatting
import net.minecraft.util.Identifier

@Serializable
data class FlagPositionInfo(
    val position: BlockPosition,
    val structureId: String?,
    val biomeId: String?,
    val blockId: String?,
    val isUnderground: Boolean,
    val isOnGround: Boolean,
    val isStart: Boolean = false,
    var entityInfo: FlagEntityInfo? = null,
) {

    fun getHints(ctx: OrienteerContext) = buildList {
        when {
            isUnderground -> Text.literal("Underground")
            else -> Text.literal("Above ground")
        }.also { add(it) }

        if (biomeId != null) {
            val identifier = Identifier(biomeId)
            Text.empty()
                .append("In biome: ")
                .append(
                    Text.translatableWithFallback("biome.minecraft.${identifier.path}", identifier.path)
                        .formatted(Formatting.DARK_GRAY)
                )
                .also { add(it) }
        }

        if (structureId != null) {
            val identifier = Identifier(structureId)
            Text.empty()
                .append("Inside structure: ")
                .append(Text.literal(identifier.path).formatted(Formatting.DARK_GRAY))
                .also { add(it) }
        }

        if (blockId != null) {
            val identifier = Identifier(blockId)
            val block = ctx.server.registryManager.get(RegistryKeys.BLOCK).get(identifier)
            Text.empty()
                .append(when {
                    isOnGround -> "On block: "
                    else -> "Under block: "
                })
                .append(
                    block?.translationKey
                        ?.let {
                            Text.translatableWithFallback(it, identifier.path).formatted(Formatting.DARK_GRAY)
                        }
                        ?: Text.literal(identifier.path).formatted(Formatting.DARK_GRAY)
                )
                .also { add(it) }
        }
    }

    fun getPoints() : Int {
        return when {
            isStart -> 0
            !isUnderground -> when {
                isOnGround -> 5
                structureId != null -> 10
                else -> 20
            }
            else -> 40
        }
    }
}
