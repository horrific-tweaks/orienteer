package me.jfenn.orienteer.map

import me.jfenn.orienteer.config.BlockPosition
import me.jfenn.orienteer.config.distanceTo
import me.jfenn.orienteer.config.toChunkPos
import me.jfenn.orienteer.map.MapRenderService.MAP_SCALE
import me.jfenn.orienteer.map.MapRenderService.MAP_SIZE
import me.jfenn.orienteer.spawn.isFluidBlock
import me.jfenn.orienteer.state.OrienteerContext
import net.minecraft.registry.RegistryKeys
import net.minecraft.server.network.ServerPlayerEntity
import net.minecraft.server.world.ServerWorld
import net.minecraft.util.math.BlockPos
import net.minecraft.util.math.ChunkPos
import net.minecraft.world.Heightmap
import net.minecraft.world.gen.GenerationStep
import org.slf4j.LoggerFactory
import kotlin.jvm.optionals.getOrNull
import kotlin.math.*
import kotlin.random.Random

object FlagService {

    val config = FlagConfig()
    private val log = LoggerFactory.getLogger(this::class.java)

    fun isValidPlacement(blockPos: BlockPos, chunkAccess: CachedChunkAccess) : Boolean {
        val blockState = chunkAccess.getBlockState(blockPos)

        if (!isEmpty(blockPos.down(), chunkAccess))
            return false

        if (!isEmpty(blockPos.down(2), chunkAccess))
            return false

        val adjacentVisible = listOf(
            blockPos.add(1, -1, 0),
            blockPos.add(-1, -1, 0),
            blockPos.add(0, -1, 1),
            blockPos.add(0, -1, -1),
        ).count {
            isEmpty(it, chunkAccess)
        }

        if (adjacentVisible < 2)
            return false

        if (!blockState.isSolidBlock(chunkAccess.world, blockPos))
            return false

        return true
    }

    fun isEmpty(blockPos: BlockPos, chunkAccess: CachedChunkAccess) : Boolean {
        val blockState = chunkAccess.getBlockState(blockPos)
        return blockState.isAir || isFluidBlock(blockState) || blockState.getCollisionShape(chunkAccess.world, blockPos).isEmpty
    }

    fun isChunkFarEnough(ctx: OrienteerContext, chunkPos: ChunkPos) : Boolean {
        val minDistance = MAP_SCALE

        // Ensure that the chunk is at least 8px away from the border of the map
        val chunkMinRange = minDistance until MAP_SIZE - minDistance
        val chunkMin = MapRenderService.getTerrainPos(Pair(chunkMinRange.first, chunkMinRange.first)).toChunkPos()
        val chunkMax = MapRenderService.getTerrainPos(Pair(chunkMinRange.last, chunkMinRange.last)).toChunkPos()
        if (chunkPos.x < chunkMin.x || chunkPos.z < chunkMin.z)
            return false
        if (chunkPos.x > chunkMax.x || chunkPos.z > chunkMax.z)
            return false

        return ctx.state.flagPositions
            .map { it.position.toChunkPos() }
            .let {
                // include the spawn position flag in this check
                // (which won't be added to the flagPositions until after every flag is known)
                it + ctx.spawnWorld.spawnPos.toChunkPos()
            }
            .all { otherChunkPos ->
                // this should ensure a distance of 16px between flags...
                chunkPos.distanceTo(otherChunkPos) > minDistance
            }
    }

    fun isBlockFarEnough(ctx: OrienteerContext, blockPos: BlockPos) : Boolean {
        return isChunkFarEnough(ctx, BlockPosition.fromBlockPos(blockPos).toChunkPos())
    }

    fun createControlPlacements(ctx: OrienteerContext, world: ServerWorld, chunkAccess: CachedChunkAccess) = sequence {
        log.info("Searching for valid flag placements...")
        val flagCount = ctx.config.flagCount

        var flagPositions by ctx.state::flagPositions
        flagPositions.clear()

        val structureIds = mutableSetOf<String>()

        val blockRegistry = ctx.server.registryManager.get(RegistryKeys.BLOCK)

        outerLoop@ for(chunkPos in MapRenderService.allMapChunks().shuffled()) {
            if (flagPositions.size >= flagCount)
                break

            val progress = ProgressInfo(
                description = "Locating flags (structure placements)...",
                currentProgress = flagPositions.size,
                maxProgress = flagCount,
            )
            yield(progress)

            // We aren't checking distance by chunk position here, as the structure origin can be in a different chunk from its pieces
            val chunk = chunkAccess.getChunk(chunkPos)

            // Look for a structure to attach to first...
            val structureRegistry = ctx.server.registryManager.get(RegistryKeys.STRUCTURE)
            for ((structure, structureStart) in chunk.structureStarts.toList().shuffled()) {
                val structureId = structureRegistry.getId(structure)?.toString()
                    ?: continue

                // If the structure has been visited already, skip it
                if (structureIds.contains(structureId)) continue

                // Set of valid blocks that flags should preferably be placed under
                val validBlocks = config.structureBlocks[structureId] ?: continue

                val isUnderground = structure.featureGenerationStep != GenerationStep.Feature.SURFACE_STRUCTURES

                // If the structure is meant to be above-ground, check that it actually is...
                // (this eliminates the completely buried/underground ruined portals that sometimes generate...)
                if (!isUnderground) {
                    val chunkHeight = chunkAccess.getBlockHeight(BlockPos(structureStart.pos.centerX, 0, structureStart.pos.centerZ), Heightmap.Type.OCEAN_FLOOR)
                    if (structureStart.boundingBox.maxY < chunkHeight)
                        continue
                }

                for (child in structureStart.children.shuffled()) {
                    val rangeX = child.boundingBox.minX..child.boundingBox.maxX
                    val rangeY = child.boundingBox.minY..child.boundingBox.maxY
                    val rangeZ = child.boundingBox.minZ..child.boundingBox.maxZ

                    for (x in rangeX) for (y in rangeY) for (z in rangeZ) {
                        yield(progress)

                        val blockPos = BlockPos(x, y, z)
                        if (!isBlockFarEnough(ctx, blockPos))
                            continue

                        val blockState = chunkAccess.getBlockState(blockPos)
                        if (!validBlocks.contains(blockState.block))
                            continue

                        if (!isValidPlacement(blockPos, chunkAccess))
                            continue

                        flagPositions.add(FlagPositionInfo(
                            position = BlockPosition.fromBlockPos(blockPos.down()),
                            structureId = structureId,
                            biomeId = null,
                            blockId = blockRegistry.getId(blockState.block).toString(),
                            isUnderground = isUnderground,
                            isOnGround = false,
                        ))
                        structureIds.add(structureId)
                        continue@outerLoop
                    }
                }
            }
        }

        outerLoop@ for(chunkPos in MapRenderService.allMapChunks().shuffled()) {
            if (flagPositions.size >= flagCount)
                break

            val progress = ProgressInfo(
                description = "Locating flags (terrain placements)...",
                currentProgress = flagPositions.size,
                maxProgress = flagCount,
            )
            yield(progress)

            // Ensure that the chunk position is not near any other controls
            if (!isChunkFarEnough(ctx, chunkPos))
                continue

            val chunk = chunkAccess.getChunk(chunkPos)

            // Try to find a valid placement on the chunk terrain...
            val chunkPositions = buildList {
                for (x in 0 until 16) for (z in 0 until 16)
                    add(Pair(x, z))
            }
            for ((x, z) in chunkPositions.shuffled()) {
                yield(progress)
                val blockPos = chunk.pos.getBlockPos(x, 0, z).mutableCopy()
                val minY = chunkAccess.getBlockHeight(blockPos, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES)
                val maxY = chunkAccess.getBlockHeight(blockPos, Heightmap.Type.MOTION_BLOCKING)

                for (y in minY+1..maxY) {
                    yield(progress)

                    blockPos.y = y
                    val blockState = chunkAccess.getBlockState(blockPos)
                    if (!config.terrainBlocks.contains(blockState.block))
                        continue

                    if (!isValidPlacement(blockPos, chunkAccess))
                        continue

                    val biome = world.getBiome(blockPos)

                    flagPositions.add(FlagPositionInfo(
                        position = BlockPosition.fromBlockPos(blockPos.down()),
                        structureId = null,
                        biomeId = biome.key.getOrNull()?.value?.toString(),
                        blockId = blockRegistry.getId(blockState.block).toString(),
                        isUnderground = false,
                        isOnGround = false,
                    ))
                    continue@outerLoop
                }
            }

            // And if there are none of those, just place it on the ground like a lazy mod developer
            val blockPos = chunk.pos.getBlockPos(Random.nextInt(16), 0, Random.nextInt(16)).mutableCopy()
            blockPos.y = chunkAccess.getBlockHeight(blockPos, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES) + 1

            val biome = world.getBiome(blockPos)

            if (isEmpty(blockPos, chunkAccess)) {
                val blockState = chunkAccess.getBlockState(blockPos.down())
                flagPositions.add(FlagPositionInfo(
                    position = BlockPosition.fromBlockPos(blockPos),
                    structureId = null,
                    biomeId = biome.key.getOrNull()?.value?.toString(),
                    blockId = blockRegistry.getId(blockState.block).toString(),
                    isUnderground = false,
                    isOnGround = true,
                ))
                continue@outerLoop
            }
        }

        // Add the first control at the spawn position
        val spawnPos = ctx.spawnWorld.spawnPos
        flagPositions.add(0, FlagPositionInfo(
            position = BlockPosition.fromBlockPos(spawnPos),
            structureId = null,
            biomeId = null,
            blockId = null,
            isUnderground = false,
            isOnGround = true,
            isStart = true,
        ))

        // Finally, sort the resulting flag list and add the start/end controls
        val centerX = flagPositions.map { it.position.x }.average()
        val centerZ = flagPositions.map { it.position.z }.average()
        flagPositions
            .sortBy {
                // This ensures that flags are sorted in a radius around the center point
                atan2(it.position.x - centerX, it.position.z - centerZ)
            }

        // Randomly decide which order the flags should be in
        if (Random.nextBoolean()) {
            flagPositions.reverse()
        }

        // reorder flags so that they start with the spawn flag
        val spawnIndex = flagPositions.indexOfFirst { it.isStart }
        flagPositions = (flagPositions.subList(spawnIndex, flagPositions.size) + flagPositions.subList(0, spawnIndex))
            .toMutableList()

        flagPositions.forEachIndexed { i, flag ->
            log.info("#$i -> isStart: ${flag.isStart}")
        }
    }

    fun spawnFlags(ctx: OrienteerContext) {
        ctx.state.flagPositions
            .dropLast(1)
            .forEachIndexed { flagIndex, flagPosition ->
                val entity = FlagEntityInfo.spawn(ctx, ctx.spawnWorld, flagPosition, flagIndex)
                flagPosition.entityInfo = entity
            }
    }

    /**
     * @return The index of the next uncaptured flag, or null if the player has captured every flag
     */
    fun getNextFlag(ctx: OrienteerContext, player: ServerPlayerEntity) : Int? {
        val flagCaptures = ctx.state.flagCaptures[player.uuidAsString] ?: emptyMap()
        return (1 until ctx.state.flagPositions.size)
            .find { flagIndex ->
                !flagCaptures.contains(flagIndex)
            }
    }

    fun getNearestFlag(ctx: OrienteerContext, player: ServerPlayerEntity) : Int? {
        val flagCaptures = ctx.state.flagCaptures[player.uuidAsString] ?: emptyMap()
        // TODO: when in SCORE mode, this needs to actually find the nearest flag

        // otherwise, get the lowest-number flag that the player has not scored
        return getNextFlag(ctx, player)
    }

}