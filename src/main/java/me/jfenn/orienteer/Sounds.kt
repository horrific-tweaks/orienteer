package me.jfenn.orienteer

import me.jfenn.orienteer.state.OrienteerContext
import net.minecraft.entity.Entity
import net.minecraft.sound.SoundCategory
import net.minecraft.sound.SoundEvents

object Sounds {

    fun playFlagCaptured(ctx: OrienteerContext, entity: Entity) {
        entity.world.playSound(
            null,
            entity.x, entity.y, entity.z,
            SoundEvents.ENTITY_PLAYER_LEVELUP,
            SoundCategory.BLOCKS,
            1f, 1f
        )
    }

    fun playFlagError(ctx: OrienteerContext, entity: Entity) {
        entity.world.playSound(
            null,
            entity.x, entity.y, entity.z,
            SoundEvents.BLOCK_NOTE_BLOCK_DIDGERIDOO.value(),
            SoundCategory.BLOCKS,
            1f, 2f
        )
    }

}