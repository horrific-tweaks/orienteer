package me.jfenn.orienteer.event

import me.jfenn.orienteer.utils.EventListener
import net.minecraft.entity.decoration.InteractionEntity
import net.minecraft.server.network.ServerPlayerEntity
import java.util.*

object InteractionEntityEvents {

    val INTERACT_LISTENERS = HashMap<UUID, (ServerPlayerEntity) -> Unit>()

    fun onInteract(entity: InteractionEntity, consumer: (ServerPlayerEntity) -> Unit) {
        INTERACT_LISTENERS[entity.uuid] = consumer
    }

    val onInteract = EventListener<Pair<InteractionEntity, ServerPlayerEntity>>()
    val onTick = EventListener<InteractionEntity>()

}