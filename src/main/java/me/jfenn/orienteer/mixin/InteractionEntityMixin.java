package me.jfenn.orienteer.mixin;

import kotlin.Pair;
import me.jfenn.orienteer.ConstantsKt;
import me.jfenn.orienteer.event.InteractionEntityEvents;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.decoration.InteractionEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(InteractionEntity.class)
abstract class InteractionEntityMixin extends Entity {

    public InteractionEntityMixin(EntityType<?> entityType, World world) {
        super(entityType, world);
    }

    @Inject(at = @At(value = "HEAD"), method = "handleAttack")
    public void handleAttack(Entity attacker, CallbackInfoReturnable<Boolean> ci) {
        if (attacker instanceof ServerPlayerEntity player) {
            var callback = InteractionEntityEvents.INSTANCE.getINTERACT_LISTENERS().get(uuid);
            if (callback != null) {
                callback.invoke(player);
                return;
            }

            InteractionEntityEvents.INSTANCE.getOnInteract().invoke(
                    new Pair<>((InteractionEntity) (Object) this, player)
            );
        }
    }

    @Inject(at = @At(value = "HEAD"), method = "interact", cancellable = true)
    public void interact(PlayerEntity player, Hand hand, CallbackInfoReturnable<ActionResult> ci) {
        if (player instanceof ServerPlayerEntity serverPlayer) {
            var callback = InteractionEntityEvents.INSTANCE.getINTERACT_LISTENERS().get(uuid);
            if (callback != null) {
                callback.invoke(serverPlayer);
                ci.setReturnValue(ActionResult.SUCCESS);
                ci.cancel();
                return;
            }

            InteractionEntityEvents.INSTANCE.getOnInteract().invoke(
                    new Pair<>((InteractionEntity) (Object) this, serverPlayer)
            );

            if (this.getCommandTags().contains(ConstantsKt.NBT_ORIENTEER_FLAG)) {
                ci.setReturnValue(ActionResult.SUCCESS);
                ci.cancel();
            }
        }
    }

    @Inject(at = @At(value = "HEAD"), method = "tick")
    public void tick(CallbackInfo ci) {
        InteractionEntityEvents.INSTANCE.getOnTick().invoke((InteractionEntity) (Object) this);
    }
}
