package me.jfenn.orienteer

// if true, item should not drop as an entity
const val NBT_ORIENTEER_VANISH = "orienteer_vanish"
const val NBT_ORIENTEER_FLAG = "orienteer_flag"
const val NBT_ORIENTEER_FLAG_ROTATED = "orienteer_flag_rotated"
const val NBT_ORIENTEER_BOOK = "orienteer_book"
