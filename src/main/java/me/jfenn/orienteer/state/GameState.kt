package me.jfenn.orienteer.state

import me.jfenn.orienteer.utils.ContextEventListener

enum class GameState(
    val color: String,
    val motd: String,
) {
    PREGAME("§a", "PRE-GAME"),
    PLAYING("§c", "IN-GAME"),
    POSTGAME("§6", "POST-GAME");

    val onEnter = ContextEventListener<OrienteerContext, GameState>()

}
