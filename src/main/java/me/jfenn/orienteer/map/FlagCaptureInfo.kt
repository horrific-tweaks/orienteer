package me.jfenn.orienteer.map

import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlinx.serialization.Serializable

@Serializable
data class FlagCaptureInfo(
    val instant: Instant = Clock.System.now(),
)
