package me.jfenn.orienteer.config

import kotlinx.serialization.Serializable
import me.jfenn.orienteer.spawn.SpawnItem
import kotlin.time.Duration
import kotlin.time.Duration.Companion.minutes

@Serializable
data class OrienteerConfig(
    var overwrite: Boolean = true,
    var timeLimit: Duration? = 60.minutes,

    // game mode settings
    var isScoreMode: Boolean = false,

    // game feature settings
    var isElytra: Boolean = false,
    var isUnlockRecipes: Boolean = true,
    var isKeepInventory: Boolean = false,
    var isPvpEnabled: Boolean = false,

    var flagCount: Int = 7,
    var hintPenalty: Int = 2,

    // spawnpoint settings
    var spawnDimension: String = "minecraft:overworld",

    var refillPlayerKitOnRespawn: Boolean = true,
    var isPlayerKit: Boolean = false,
    var playerKitItems: List<SpawnItem> = listOf(
        SpawnItem("minecraft:iron_chestplate"),
        SpawnItem("minecraft:iron_pickaxe"),
        SpawnItem("minecraft:iron_axe"),
        SpawnItem("minecraft:shield"),
        SpawnItem("minecraft:torch", count = 64),
    ),

    var autoShutDownInPostgame: Boolean = true,
    var unsafeSkipWorldClose: Boolean = false,
)
