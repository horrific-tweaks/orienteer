package me.jfenn.orienteer.config

import me.jfenn.orienteer.MOD_ID

object ConfigService {

    var config: OrienteerConfig by config("$MOD_ID/config.json")

}